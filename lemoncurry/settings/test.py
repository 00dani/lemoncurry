from .base import *

ALLOWED_HOSTS = ['*']
SECURE_SSL_REDIRECT = False
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
